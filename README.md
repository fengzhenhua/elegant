# elegant

由于[elegantbook项目](https://github.com/ElegantLaTeX/ElegantBook)已经停止维护，同时鉴于其优美的设计元素，本人借鉴了其封面的设计风格，重新实现了这个封面，尽可能使用原生LaTeX语言，以使未来不需要大的维护也能使用。服务于大众，这个小项目作为完全开源的宏包，任何人可以使用和修改，但是请勿商用，仅限个人作品使用。如若商用请联系本人取得授权。

# 信息 #

- 项目： elegantpage.sty
- 作者： 冯振华
- 版本： V1.0
- 邮箱:  fengzhenhua@outlook.com
- 版权： 个人完全开源
- 日期： 2023年05月17日
